/*Implemente um algoritmo que pegue duas matrizes (array de arrays) e
realize sua multiplicação. Lembrando que para realizar a multiplicação
dessas matrizes o número de colunas da primeira matriz tem que ser
igual ao número de linhas da segunda matriz. (2x2)
a. Caso teste 1 : [ [ [2],[-1] ], [ [2],[0] ] ] e [ [2,3],[-2,1] ] multiplicadas dão [ [6,5], [4,6] ]
b. Caso teste 2 : [ [4,0], [-1,-1] ] e [ [-1,3], [2,7] ] multiplicadas dão [ [-4,12], [-1,-10] ]*/

// Algoritmo da atividade
function multiplicaMatriz(matrizA, matrizB) {
    var resultado = [];
    for (let i = 0; i < matrizA.length; i++) { // Loop para pegar cada linha da Matriz A.
      let linha = matrizA[i];
      let linhaAux = [];
      for (let j = 0; j < matrizB[0].length; j++) { // Loop para pegar cada coluna da Matriz B.
        let soma = 0;
        for (let k = 0; k < matrizB.length; k++) {
          soma += linha[k] * matrizB[k][j];
        }
        linhaAux.push(soma);
      }
      resultado.push(linhaAux);
    }
    return resultado;
  }

// Matrizes para o teste
var matrizA = [[4,0], [-1,-1]]
var matrizB = [[-1,3], [2,7]]
var resultado = []

// Verifica se os tamanhos das matrizes são iguais e com dimensões 2x2.
if(matrizA.length == matrizB.length & matrizA[0].length == matrizB[0].length & matrizA.length == 2 & matrizA[0].length == 2){
    resultado = multiplicaMatriz(matrizA, matrizB)
}
console.log(resultado)
