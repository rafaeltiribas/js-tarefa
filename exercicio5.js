/*Teste 5 números inteiros aleatórios. Os testes:
● Caso o valor seja divisível por 3, imprima no console “fizz”.
● Caso o valor seja divisível por 5 imprima “buzz”.
● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
“fizzbuzz”.
● Caso contrário imprima nada.*/

var numero
for(var i = 0; i < 5; i++){
    numero = Math.floor(Math.random()*100)
    console.log(numero)
    if((numero%3)== 0 & (numero%5) == 0){
        console.log("fizzbuzz")
    }else{
        if((numero%3)== 0){
            console.log("fizz")
        }else{
            if((numero%5)== 0){
                console.log("buzz")
            }
        }
    }
}