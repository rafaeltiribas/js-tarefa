/*Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
média aritmética dessas notas for maior ou igual que 6 imprima
“Aprovado”, caso contrário “Reprovado”.*/

let nota1 = prompt("Digite a primeira nota: ")
let nota2 = prompt("Digite a segunda nota: ")
let nota3 = prompt("Digite a terceira nota: ")

nota1 = parseInt(nota1)
nota2 = parseInt(nota2)
nota3 = parseInt(nota3)

function verificaAprovacao(nota1, nota2, nota3){
    if(((nota1+nota2+nota3)/3) >= 6){
        console.log("Aprovado")
    }else{
        console.log("Reprovado")
    }
}

verificaAprovacao(nota1, nota2, nota3)

