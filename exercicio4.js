/*Vocês receberão um arquivo com um array de objetos representando
deuses do jogo Smite. Usando os métodos aprendidos em aula, faça os
seguintes exercícios:
● Q1. Imprima o nome e a quantidade de features de todos os deuses usando
uma única linha de código.
● Q2. Imprima todos os deuses que possuem o papel de "Mid"
● Q3. Organize a lista pelo panteão do deus.
● Q4. Faça um código que retorne um novo array com o nome de cada deus e
entre parênteses, a sua classe.
Por exemplo, o array deverá ficar assim: ["Achilles (Warrior)", "Agni (Mage)", ...]*/

//Os algoritmos a seguir estão no final do arquivo fornecido "arquivo_exercicio_4" para que possam ser testados.

//● Q1. Imprima o nome e a quantidade de features de todos os deuses usando uma única linha de código.
for(var i = 0; i < gods.length; i++){console.log("Nome:" + gods[i].name + "Quantidade de features:" + gods[i].features.length)}

//● Q2. Imprima todos os deuses que possuem o papel de "Mid"
for(var i = 0; i<gods.length; i++){
    for(var j = 0; j < gods[0].roles.length; j++){
      if(gods[i].roles[j] == "Mid"){
        console.log(gods[i].name)
      }
    }
}

//● Q3. Organize a lista pelo panteão do deus.
gods.sort(function(a, b){
  if(a.pantheon > b.pantheon){
    return 1
  }else{
    return -1
  }
})
console.log(gods)

/*● Q4. Faça um código que retorne um novo array com o nome de cada deus e entre parênteses, a sua classe.
Por exemplo, o array deverá ficar assim: ["Achilles (Warrior)", "Agni (Mage)", ...]*/

var gods2 = []
for(var i = 0; i < gods.length; i++){
  var aux = []
  aux.push(gods[i].name)
  aux.push("("+gods[i].class+")")
  gods2.push(aux)
}

console.log(gods2)