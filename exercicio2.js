/* Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
calcular e escrever o valor correspondente em graus Celsius.*/

function emCelsius(temperatura){
    temperatura = ((temperatura-32)/9)*5
    return parseInt(temperatura)
}

let temperatura = prompt("Insira uma temperatura em Fahrenheit que deseja converter para Celsius: ")
temperatura = parseInt(temperatura)
temperatura = emCelsius(temperatura)
console.log("Esta temperatura corresponde a " + temperatura + " graus Celsius.")